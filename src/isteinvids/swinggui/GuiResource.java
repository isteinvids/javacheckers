package isteinvids.swinggui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 * @author EmirRhouni
 */
public final class GuiResource {

    private static final List<GuiResource> cache;
    private final String name;
    private final String url;
    private BufferedImage image = null;

    static {
        cache = new ArrayList();
    }

    private GuiResource(final String name0, final String url0) {
        this.name = name0;
        this.url = url0;
        new Thread("GuiResource downloading thread " + name0) {
            @Override
            public void run() {
                File cachefile = null;
                new File("cache").mkdir();
                cachefile = new File("cache" + File.separator + name.hashCode() + ".png");
                if (cachefile.exists()) {
                    try {
                        image = ImageIO.read(cachefile);
                    } catch (Exception e) {
                        System.err.println("Couldn't read GuiResource from file: " + name0 + ", " + url0);
                        e.printStackTrace();
                    }
                }
                try {
                    image = ImageIO.read(new URL(url));
                    if (image != null) {
                        ImageIO.write(image, "png", cachefile);
                    }
                } catch (Exception e) {
                    System.err.println("Couldn't write GuiResource to file: " + name0 + ", " + url0);
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public String getUrl() {
        return url;
    }

    public BufferedImage getImage() {
        return image;
    }

    public static GuiResource create(String name, String url) {
        for (GuiResource t1 : cache) {
            if (t1.name.equals(name)) {
                return t1;
            }
        }
//        ModLauncher.printMessage(url);
        GuiResource ret = new GuiResource(name, url);
        cache.add(ret);
        return ret;
    }
}
