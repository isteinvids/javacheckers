package isteinvids.swinggui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author EmirRhouni
 */
public class GuiButton implements Gui {

    private int width = 325, height = 35;
    private static BufferedImage sprite = null;
    private static Image[] sps = null;
    public String display;
    private boolean enabled;
    private int x;
    private int y;
    private boolean selected = false;
    private boolean drawButton = true;
    private Runnable event;

    public GuiButton() {
        this("");
    }

    public GuiButton(String display) {
        this.display = display;
        this.enabled = true;

        if (sprite == null) {
            try {
                sprite = ImageIO.read(GuiButton.class.getResourceAsStream("/widgets.png"));
                sps = new Image[]{
                    sprite.getSubimage(0, 46, 200, 20),//.getScaledInstance(width, height, 1), // disabled
                    sprite.getSubimage(0, 66, 200, 20),//.getScaledInstance(width, height, 1), // normal
                    sprite.getSubimage(0, 86, 200, 20)//.getScaledInstance(width, height, 1) // selected
                };
            } catch (IOException ex) {
                Logger.getLogger(GuiButton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void render(GameContainer gc, Graphics2D g) {
        if (this.isDrawButton()) {
            g.drawImage(this.enabled ? (this.selected ? sps[2] : sps[1]) : sps[0], x, y, width, height, null);
            int w = Math.round((float) g.getFont().getStringBounds(display, g.getFontRenderContext()).getWidth());
            g.setColor(Color.black);
            g.drawString(display, (x + (width / 2) - (w / 2)) + 1, y + (height / 1.7f) + 1);
            g.setColor(enabled ? (selected ? new Color(1.0F, 1.0F, 0.6F) : Color.white) : Color.darkGray);
            g.drawString(display, (x + (width / 2) - (w / 2)), y + (height / 1.7f));
        }
    }

    @Override
    public void update(GameContainer gc) {
        selected = false;
        if (this.isDrawButton() && enabled) {
            if (gc.getInput().getMouseX() > x && gc.getInput().getMouseX() < x + width) {
                if (gc.getInput().getMouseY() > y && gc.getInput().getMouseY() < y + height) {
                    selected = true;
                    if (gc.getInput().isMousePressed(MouseEvent.BUTTON1)) {
                        if (event != null) {
                            event.run();
                        }
                    }
                }
            }
        }
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEvent(Runnable event) {
        this.event = event;
    }

    public boolean isDrawButton() {
        return drawButton;
    }

    public void setDrawButton(boolean drawButton) {
        this.drawButton = drawButton;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
