package isteinvids.swinggui;

import java.awt.Graphics2D;

/**
 *
 * @author EmirRhouni
 */
public interface Gui {

    public void render(GameContainer gc, Graphics2D g);

    public void update(GameContainer gc);
}
