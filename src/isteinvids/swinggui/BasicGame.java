/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package isteinvids.swinggui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 *
 * @author EmirRhouni
 */
public abstract class BasicGame {

    private boolean gameRunning = false;
//    private final JPanel inputPanel = new JPanel)
    private final Input input;
    private final GameContainer gameContainer;
    protected final JFrame frame;
    protected final JPanel panel = new JPanel() {
        @Override
        protected void paintComponent(Graphics g) {
            try {
                g.setColor(Color.black);
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
                g.setColor(Color.white);
                render(gameContainer, (Graphics2D) (g));
            } catch (GameException ex) {
                Logger.getLogger(BasicGame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };

    public BasicGame(JFrame fr2) {
        frame = fr2 == null ? new JFrame() : fr2;
        input = new Input(null);
        gameContainer = new GameContainer(this, input);
    }

    public BasicGame() {
        this((JFrame) null);
    }

    public GameContainer getGameContainer() {
        return gameContainer;
    }

    private void initFrame() throws GameException {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }
        frame.getContentPane().removeAll();
        frame.getContentPane().repaint();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel.setFocusable(true);
        addInputToPanel(panel);

        frame.getContentPane().add(panel);
        frame.repaint();
        init(gameContainer);
    }

    public void addInputToPanel(JPanel panel1) {
        panel1.addKeyListener(input);
        panel1.addMouseListener(input);
        panel1.addMouseMotionListener(input);
    }

    public void start() {
        try {
            initFrame();
        } catch (GameException ex) {
            Logger.getLogger(BasicGame.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!gameRunning) {
            gameRunning = true;
            frame.setVisible(true);
            Thread loop = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        gameLoop();
                    } catch (GameException ex) {
                        Logger.getLogger(BasicGame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }, "GameLoop");
            loop.start();
        }
    }

    //Only run this in another Thread!
    private void gameLoop() throws GameException {
        final double GAME_HERTZ = 30.0;
        final double TIME_BETWEEN_UPDATES = 900000000 / GAME_HERTZ;
        final int MAX_UPDATES_BEFORE_RENDER = 5;
        double lastUpdateTime = System.nanoTime();
        double lastRenderTime = System.nanoTime();

        final double TARGET_FPS = 60;
        final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

        int lastSecondTime = (int) (lastUpdateTime / 1000000000);

        while (gameRunning) {
            double now = System.nanoTime();
            int updateCount = 0;

            while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER && gameRunning) {
                update(gameContainer);
                lastUpdateTime += TIME_BETWEEN_UPDATES;
                updateCount++;
            }

            if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
                lastUpdateTime = now - TIME_BETWEEN_UPDATES;
            }
//            if (!panel.isFocusOwner()) {
//                panel.setFocusable(true);
//                panel.requestFocus();
//                panel.requestFocusInWindow();
//            }
            frame.repaint();
            lastRenderTime = now;

            int thisSecond = (int) (lastUpdateTime / 1000000000);
            if (thisSecond > lastSecondTime) {
                lastSecondTime = thisSecond;
            }

            while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES && gameRunning) {
                try {
                    Thread.sleep(2);
                } catch (Exception e) {
                }

                now = System.nanoTime();
            }
        }
    }

    public abstract void init(GameContainer gc) throws GameException;

    public abstract void render(GameContainer gc, Graphics2D g) throws GameException;

    public abstract void update(GameContainer gc) throws GameException;

    public void drawCenteredString(String text, int x, int y, Graphics2D g) {
        int w = Math.round((float) g.getFont().getStringBounds(text, g.getFontRenderContext()).getWidth());
        Color col = g.getColor();
        g.setColor(Color.black);
        g.drawString(text, x - (w / 1.85f) + 1, y + 1);
        g.setColor(col);
        g.drawString(text, x - (w / 1.85f), y);
    }

    public boolean drawClickableCenteredString(String text, int x, int y, Graphics2D g) {
        return this.drawClickableCenteredString(text, x, y, null, g);
    }

    public boolean drawClickableCenteredString(String text, int x, int y, Color tint, Graphics2D g) {
        int w = Math.round((float) g.getFont().getStringBounds(text, g.getFontRenderContext()).getWidth());
        Color col = g.getColor();
        float nx = x - (w / 1.85f);
        boolean ontop = false;
        boolean ret = false;
        if (getGameContainer().getInput().getMouseX() > x - (w / 2) && getGameContainer().getInput().getMouseX() < x + (w / 2)) {
            if (getGameContainer().getInput().getMouseY() > y - 15 && getGameContainer().getInput().getMouseY() < y + 4) {
                ontop = true;
                if (getGameContainer().getInput().isMousePressed(MouseEvent.BUTTON1)) {
                    ret = true;
                }
            }
        }
        g.setColor(Color.black);
        g.drawString(text, nx + 1, y + 1);
        g.setColor(ontop && tint != null ? tint : col);
        g.drawString(text, nx, y);
        return ret;
    }

    public void drawString(String text, int x, int y, Graphics2D g) {
        Color col = g.getColor();
        g.setColor(Color.black);
        g.drawString(text, x + 1, y + 1);
        g.setColor(col);
        g.drawString(text, x, y);
    }

    public JFrame getFrame() {
        return frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y) {
        return this.drawClickableImage(g, image, x, y, image.getWidth(null), image.getHeight(null), null);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, Color tint) {
        return this.drawClickableImage(g, image, x, y, image.getWidth(null), image.getHeight(null), tint);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height) {
        return this.drawClickableImage(g, image, x, y, width, height, MouseEvent.BUTTON1, null);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height, Color tint) {
        return this.drawClickableImage(g, image, x, y, width, height, MouseEvent.BUTTON1, tint);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height, int mouseButton, Color tint) {
        g.drawImage(image, x, y, width, height, null);
        GameContainer gc = getGameContainer();
        if (gc.getInput().getMouseX() > x && gc.getInput().getMouseY() > y) {
            if (gc.getInput().getMouseX() < x + width && gc.getInput().getMouseY() < y + height) {
                if (tint != null) {
                    g.setColor(tint);
                    g.fillRect(x, y, width, height);
                }
                if (gc.getInput().isMousePressed(mouseButton)) {
                    return true;
                }
            }
        }
        return false;
    }
}
