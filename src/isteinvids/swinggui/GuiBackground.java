package isteinvids.swinggui;

import java.awt.Graphics2D;
import java.awt.Image;

/**
 *
 * @author EmirRhouni
 */
public class GuiBackground implements Gui {

    public static enum BackgroundType {

        NORMAL, TILE, STRETCH_1, STRETCH_2, CENTER;
    }
    private final Image background;
    private final BackgroundType backgroundType;

    public GuiBackground(Image background, BackgroundType backgroundType) {
        this.background = background;
        this.backgroundType = backgroundType;
    }

    @Override
    public void render(GameContainer gc, Graphics2D g) {
        if (backgroundType == BackgroundType.NORMAL) {
            g.drawImage(background, 0, 0, null);
        }
        if (backgroundType == BackgroundType.TILE) {
            for (int i = 0; i <= (gc.getWidth() / background.getWidth(null)); i++) {
                for (int j = 0; j <= (gc.getHeight() / background.getHeight(null)); j++) {
                    g.drawImage(background, i * background.getWidth(null), j * background.getHeight(null), null);
                }
            }
        }
        if (backgroundType == BackgroundType.STRETCH_1) {
            g.drawImage(background, 0, 0, gc.getWidth(), gc.getHeight(), null);
        }
        if (backgroundType == BackgroundType.STRETCH_2) {
            int origwidth = background.getWidth(null);
            int origheight = background.getHeight(null);
            int posx = 0, posy = 0;
            int sizex = gc.getWidth(), sizey = gc.getHeight();

            if (sizey >= gc.getHeight()) {
                float widthdif =(float)gc.getWidth()/ (float)origwidth;
                sizey = (int) ((origheight)*widthdif);
//                posy = -(sizey/2);
            }
            if (sizey < gc.getHeight()) {
                sizey = gc.getHeight();
            }
            
            g.drawImage(background, posx, posy, sizex, sizey, null);
        }
        if (backgroundType == BackgroundType.CENTER) {
            g.drawImage(background, (gc.getWidth() / 2) - (background.getWidth(null) / 2), (gc.getHeight() / 2) - (background.getHeight(null) / 2), null);
        }
    }

    @Override
    public void update(GameContainer gc) {
    }
}
