/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package isteinvids.swinggui;

/**
 *
 * @author EmirRhouni
 */
public class GameException extends Exception {

    public GameException() {
    }

    public GameException(String message) {
        super(message);
    }

    public GameException(Throwable cause) {
        super(cause);
    }

    public GameException(String message, Throwable cause) {
        super(message, cause);
    }
}
