package isteinvids.swinggui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 *
 * @author EmirRhouni
 */
public class GuiTextbox implements Gui {

    public static int selected = -1;
    private int width = 300, height = 35;
    private final int id;
    private int x;
    private int y;
    private int tick;
    private double scale = 1.0;
    private String text;
    private boolean drawButton = true;
    private boolean registered = false;
    private boolean password = false;
    private boolean enabled = true;

    public GuiTextbox(int id) {
        text = "";
        this.id = id;
    }

    @Override
    public void render(GameContainer gc, Graphics2D g) {
        if (drawButton) {
            String pass = password ? "" : text;
            if (password) {
                for (char c : text.toCharArray()) {
                    pass += "*";
                }
            }
            int w = (int) Math.round(Math.round((float) g.getFont().getStringBounds(pass + " ", g.getFontRenderContext()).getWidth()) * scale);
            g.setColor(enabled ? new Color(0.5f, 0.5f, 0.5f, 0.6f) : new Color(0.3f, 0.3f, 0.3f, 0.6f));
            g.fillRect(x, y, width, height);
            g.setColor(Color.lightGray);
            g.scale(scale, scale);
            g.drawString(pass, (int) ((x + 5) * (1 / scale)), (int) ((y + (height / 1.7f)) * (1 / scale)));
            g.scale(1 / scale, 1 / scale);
            tick++;
            if (tick > 60) {
                tick = 0;
            }
            if (enabled && selected == id) {
                g.setColor(tick > 30 ? Color.white : new Color(0, 0, 0, 0));
                g.fillRect(x + w, (int) (y - 13 + (height / 1.7f)), 2, 20);
            }
//            g.drawString("|", x + 7 + w, y - 1 + (HEIGHT / 1.7f));
        }
    }

    public int getId() {
        return id;
    }

    public boolean isSelected() {
        return this.id == selected;
    }

    @Override
    public void update(GameContainer gc) {
        if (!registered) {
            gc.getInput().registerTextEvent(this, new InputTextEvent() {
                @Override
                public void textEnteredEvent(GuiTextbox box, char c) {
                    if (box.drawButton && box.enabled && selected == id) {
//                        if () {
                            /*1
                         || c == '@' || c == '.' || c == ',' || c == '/'
                         || c == '-' || c == '+' || c == '=' || c == '_'
                         || c == '\b' || c == ' '*/
                        if ((box.text.length() <= 0 && c == '\b') || (box.text.length() > 256 && c != '\b')) {
                            return;
                        }
                        box.text = c == '\b' ? box.text.substring(0, box.text.length() - 1) : box.text + c;
//                        }
                    }
                }
            });
            registered = true;
        }

        if (this.enabled) {
//            ModLauncher.printMessage("this " + id + ", " + x + ", " + y);
            if (gc.getInput().getMouseX() > x && gc.getInput().getMouseX() < x + width
                    && gc.getInput().getMouseY() > y && gc.getInput().getMouseY() < y + height) {
                if (gc.getInput().isMousePressed(MouseEvent.BUTTON1)) {
                    selected = this.id;
                }
            } else {
//                if (gc.getInput().isMousePressed(MouseEvent.BUTTON1)) {
//                    SELECTED_BOX = -1;
//                }
            }
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDrawingButton() {
        return drawButton;
    }

    public void setDrawButton(boolean drawButton) {
        this.drawButton = drawButton;
    }

    public void setPassword(boolean password) {
        this.password = password;
    }

    public boolean isPassword() {
        return password;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setSize(int width, int height, double scale) {
        this.width = width;
        this.height = height;
        this.scale = scale;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

}
