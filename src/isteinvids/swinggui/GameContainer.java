package isteinvids.swinggui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author EmirRhouni
 */
public class GameContainer {

    private final BasicGame game;
    private final Input input;

    public GameContainer(BasicGame game, Input input) {
        this.game = game;
        this.input = input;
    }

    public BasicGame getGame() {
        return game;
    }

    public Input getInput() {
        return input;
    }

    public void setResizable(boolean resize) {
        game.frame.setResizable(resize);
    }

    public JFrame getFrame() {
        return game.frame;
    }

    public boolean isResizable() {
        return game.frame.isResizable();
    }

    public void setTitle(String title) {
        game.frame.setTitle(title);
    }

    public String getTitle() {
        return game.frame.getTitle();
    }

    public void setIcon(String ref) {
        try {
            game.frame.setIconImage(ImageIO.read(BasicGame.class.getResource("/" + ref)));
        } catch (IOException ex) {
            Logger.getLogger(BasicGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getWidth() {
        return game.frame.getWidth();
    }

    public int getHeight() {
        return game.frame.getHeight();
    }

    public void setSize(int width, int height) {
        game.frame.setSize(width, height);
        game.frame.setLocationRelativeTo(null);
    }

    public void setVisible(boolean v) {
        this.game.frame.setVisible(v);
    }

    public BufferedImage readImage(String file) throws GameException {
        try {
            return ImageIO.read(this.getClass().getResourceAsStream("/" + file));
        } catch (IOException ex) {
            throw new GameException(ex);
        }
    }
}
