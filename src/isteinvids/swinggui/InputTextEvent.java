package isteinvids.swinggui;

/**
 *
 * @author EmirRhouni
 */
public interface InputTextEvent {

    public void textEnteredEvent(GuiTextbox box, char c);
}
