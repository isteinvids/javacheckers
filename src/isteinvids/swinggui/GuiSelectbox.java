package isteinvids.swinggui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author EmirRhouni
 */
public class GuiSelectbox implements Gui {

    private static final int WIDTH = 325, HEIGHT = 35;
    private static BufferedImage sprite = null;
    private static Image[] sps = null;
    private boolean enabled;
    private int x;
    private int y;
    private boolean selected = false;
    private boolean drawButton = true;
    private Runnable event;

    public GuiSelectbox() {
        this.enabled = true;

        if (sprite == null) {
            try {
                sprite = ImageIO.read(GuiButton.class.getResourceAsStream("/widgets.png"));
                sps = new Image[]{
                    sprite.getSubimage(0, 46, 200, 20).getScaledInstance(WIDTH, HEIGHT, 1), // disabled
                    sprite.getSubimage(0, 66, 200, 20).getScaledInstance(WIDTH, HEIGHT, 1), // normal
                    sprite.getSubimage(0, 86, 200, 20).getScaledInstance(WIDTH, HEIGHT, 1) // selected
                };
            } catch (IOException ex) {
                Logger.getLogger(GuiButton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void render(GameContainer gc, Graphics2D g) {
        if (this.isDrawButton()) {
            g.drawImage(this.enabled ? (this.selected ? sps[2] : sps[1]) : sps[0], x, y, null);
        }
    }

    @Override
    public void update(GameContainer gc) {
        selected = false;
        if (this.isDrawButton() && enabled) {
            if (gc.getInput().getMouseX() > x && gc.getInput().getMouseX() < x + WIDTH) {
                if (gc.getInput().getMouseY() > y && gc.getInput().getMouseY() < y + HEIGHT) {
                    selected = true;
                    if (gc.getInput().isMousePressed(MouseEvent.BUTTON1)) {
                        if (event != null) {
                            event.run();
                        }
                    }
                }
            }
        }
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setEvent(Runnable event) {
        this.event = event;
    }

    public boolean isDrawButton() {
        return drawButton;
    }

    public void setDrawButton(boolean drawButton) {
        this.drawButton = drawButton;
    }
}
