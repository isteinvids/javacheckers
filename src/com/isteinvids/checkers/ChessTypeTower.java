package com.isteinvids.checkers;

import isteinvids.swinggui.Position;

/**
 *
 * @author Emir
 */
public class ChessTypeTower extends ChessTypes {

    protected ChessTypeTower(String colour) {
        super("TOWER", colour);
    }

    @Override
    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        boolean[][] toret = new boolean[8][8];
        boolean flag = false;
        for (int i = pos.getX(); i >= 0; i--) {
            if(sameColourOn(i, pos.getY(),pos))flag=true;
            if (!flag) {
                if (pos.getX() != i) {
                    toret[i][pos.getY()] = true;
                }
            }
            if (collisions[i][pos.getY()]) {
                flag = true;
            }
        }
        flag = false;
        for (int j = pos.getY(); j >= 0; j--) {
            if(sameColourOn(pos.getX(), j,pos))flag=true;
            if (!flag) {
                if (pos.getY() != j) {
                    toret[pos.getX()][j] = true;
                }
            }
            if (collisions[pos.getX()][j]) {
                flag = true;
            }
        }
        flag=false;
        for (int i = pos.getX(); i < 8; i++) {
            if(sameColourOn(i, pos.getY(),pos))flag=true;
            if (!flag) {
                if (pos.getX() != i) {
                    toret[i][pos.getY()] = true;
                }
            }
            if (collisions[i][pos.getY()]) {
                flag = true;
            }
        }
        flag = false;
        for (int j = pos.getY(); j < 8; j++) {
            if(sameColourOn(pos.getX(), j,pos))flag=true;
            if (!flag) {
                if (pos.getY() != j) {
                    toret[pos.getX()][j] = true;
                }
            }
            if (collisions[pos.getX()][j]) {
                flag = true;
            }
        }
        return toret;
    }

}
