package com.isteinvids.checkers;

import isteinvids.swinggui.Position;

/**
 *
 * @author Emir
 */
public class ChessTypeBishop extends ChessTypes {

    protected ChessTypeBishop(String colour) {
        super("BISHOP", colour);
    }
    
    @Override
    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        boolean[][] toret = new boolean[8][8];
        boolean flag = false;
        for (int i = 0; i < 8 - pos.getX() && i < 8 - pos.getY(); i++) {
            if(sameColourOn(pos.getX()+i, pos.getY()+i,pos))flag=true;
            if (!flag) {
                toret[pos.getX() + i][pos.getY() + i] = true;
            }
            if (collisions[pos.getX() + i][pos.getY() + i]) {
                flag = true;
            }
        }
        flag = false;
        for (int i = 0; i <= Math.min(pos.getX(), pos.getY()); i++) {
            if(sameColourOn(pos.getX()-i, pos.getY()-i,pos))flag=true;
            if (!flag) {
                toret[pos.getX() - i][pos.getY() - i] = true;
            }
            if (collisions[pos.getX() - i][pos.getY() - i]) {
                flag = true;
            }
        }
        flag = false;
        for (int i = 0; i < 8 - pos.getX(); i++) {
            if(sameColourOn(pos.getX()+i, pos.getY()-i,pos))flag=true;
            if (pos.getY() - i >= 0) {
                if (!flag) {
                    toret[pos.getX() + i][pos.getY() - i] = true;
                }
                if (collisions[pos.getX() + i][pos.getY() - i]) {
                    flag = true;
                }
            }
        }
        flag = false;
        for (int i = 0; i < 8 - pos.getY(); i++) {
            if(sameColourOn(pos.getX()-i, pos.getY()+i,pos))flag=true;
            if (pos.getX() - i >= 0) {
                if (!flag) {
                    toret[pos.getX() - i][pos.getY() + i] = true;
                }
                if (collisions[pos.getX() - i][pos.getY() + i]) {
                    flag = true;
                }
            }
        }
        return toret;
    }

}
