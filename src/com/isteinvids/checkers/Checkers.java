package com.isteinvids.checkers;

import isteinvids.swinggui.BasicGame;
import isteinvids.swinggui.GameContainer;
import isteinvids.swinggui.GameException;
import isteinvids.swinggui.Position;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseEvent;

/**
 *
 * @author Emir
 */
public class Checkers extends BasicGame {

    public static void main(String[] args) {
        instance = new Checkers();
        instance.getGameContainer().setSize((size * tiles), (size * tiles)+120);
        instance.getGameContainer().setResizable(false);
        instance.start();
    }
    private static Checkers instance;
    private static final int tiles = 8;
    private static final int size = 64;
    private static final int image_size = 45;
    private final ChessTypes[][] chessTypeseses = new ChessTypes[tiles][tiles];
    //                           ^^ Netbeans suggested this name, don't judge
    private Position selected0 = null;
    private String turn = "white";
    private boolean[][] canMoveTo = null;

    public static int getCorrectIncrementBy(int to) {
        return to >= 0 ? +1 : -1;
    }

    public static Checkers getInstance() {
        return instance;
    }

    @Override
    public void init(GameContainer gc) throws GameException {
        chessTypeseses[0][0] = ChessTypes.BLACK_TOWER;
        chessTypeseses[1][0] = ChessTypes.BLACK_KNIGHT;
        chessTypeseses[2][0] = ChessTypes.BLACK_BISHOP;
        chessTypeseses[3][0] = ChessTypes.BLACK_QUEEN;
        chessTypeseses[4][0] = ChessTypes.BLACK_KING;
        chessTypeseses[5][0] = ChessTypes.BLACK_BISHOP;
        chessTypeseses[6][0] = ChessTypes.BLACK_KNIGHT;
        chessTypeseses[7][0] = ChessTypes.BLACK_TOWER;

        chessTypeseses[0][tiles - 1] = ChessTypes.WHITE_TOWER;
        chessTypeseses[1][tiles - 1] = ChessTypes.WHITE_KNIGHT;
        chessTypeseses[2][tiles - 1] = ChessTypes.WHITE_BISHOP;
        chessTypeseses[3][tiles - 1] = ChessTypes.WHITE_KING;
        chessTypeseses[4][tiles - 1] = ChessTypes.WHITE_QUEEN;
        chessTypeseses[5][tiles - 1] = ChessTypes.WHITE_BISHOP;
        chessTypeseses[6][tiles - 1] = ChessTypes.WHITE_KNIGHT;
        chessTypeseses[7][tiles - 1] = ChessTypes.WHITE_TOWER;

        for (int i = 0; i < tiles; i++) {
            chessTypeseses[i][1] = ChessTypes.BLACK_PAWN;
            chessTypeseses[i][tiles - 2] = ChessTypes.WHITE_PAWN;
        }
    }

    @Override
    public void render(GameContainer gc, Graphics2D g) throws GameException {
        //pls fix
        boolean flag = false;
        for (int i = 0; i < tiles; i++) {
            if (tiles % 2 == 0) {
                flag = !flag;
            }
            for (int j = 0; j < tiles; j++) {
                flag = !flag;
                if (flag) {
                    g.setColor(new Color(1.0F, 0.9F, 0.8F, 1.0F));
                    g.fillRect(i * size, j * size, size, size);
                } else {
                    g.setColor(new Color(0.9F, 0.6F, 0.3F, 1.0F));
                    g.fillRect(i * size, j * size, size, size);
                }
            }
        }

        if (canMoveTo != null) {
            for (int i = 0; i < tiles; i++) {
                for (int j = 0; j < tiles; j++) {
                    if (canMoveTo[i][j]) {
                        g.setColor(new Color(0.0F, 1.0F, 0.0F, 0.5F));
                        g.fillRect(i * size, j * size, size, size);
                    }
                }
            }
        }

        if (selected0 != null) {
            Stroke oldStroke = g.getStroke();
            g.setColor(Color.red);
            g.setStroke(new BasicStroke(5.0F));
            g.drawRect(selected0.getX() * size, selected0.getY() * size, size, size);
            g.setStroke(oldStroke);
        }

        for (int i = 0; i < chessTypeseses.length; i++) {
            for (int j = 0; j < chessTypeseses[i].length; j++) {
                g.setColor(Color.black);
                g.drawString(i + "," + j, (i * size) + 4, (j * size) + 14);
                g.setColor(Color.white);
                g.drawString(i + "," + j, (i * size) + 3, (j * size) + 13);
                if (chessTypeseses[i][j] != null) {
                    String name = chessTypeseses[i][j].name();
                    if (chessTypeseses[i][j].getImage() == null) {
                        g.setColor(Color.black);
                        g.drawString(name, (i * size) + (size / 2) + 1, (j * size) + (size / 2) + 1);
                        g.setColor(Color.white);
                        g.drawString(name, (i * size) + (size / 2), (j * size) + (size / 2));
                    } else {
                        g.drawImage(chessTypeseses[i][j].getImage(), (i * size) + ((size - image_size) / 2), (j * size) + ((size - image_size) / 2), null);
                    }
                }
            }
        }
    }

    @Override
    public void update(GameContainer gc) throws GameException {
        if (gc.getInput().isMousePressed(MouseEvent.BUTTON1)) {

            if (gc.getInput().getMouseX() >= 0 && gc.getInput().getMouseY() >= 0) {
                if (gc.getInput().getMouseX() < tiles * size && gc.getInput().getMouseY() < tiles * size) {
                    Position selectedPos = new Position(gc.getInput().getMouseX() / size, gc.getInput().getMouseY() / size);
                    ChessTypes type = getChessTypeAt(selectedPos.getX(), selectedPos.getY());
                    if (selected0 == null) {
                        if (type != null) {
                            if (type.getColour().equals(turn)) {
                                canMoveTo = type.canMoveTo(selectedPos, getCollisionsFor(selectedPos));
                                selected0 = selectedPos;
                                if (canMoveTo != null) {
                                    canMoveTo[selectedPos.getX()][selectedPos.getY()] = false;
                                }
                            }
                        }
                    } else {
                        moveTo(selected0, selectedPos);
                        selected0 = null;
                        canMoveTo = null;
                    }
                }
            }
        }
    }

    public ChessTypes getChessTypeAt(int x, int y) {
        if (x >= 0 && x < chessTypeseses.length) {
            if (y >= 0 && y < chessTypeseses[x].length) {
                return chessTypeseses[x][y];
            }
        }
        return null;
    }

    private void moveTo(Position from, Position to) {
        ChessTypes chs = chessTypeseses[from.getX()][from.getY()];
        if (chs != null) {
            if (canMoveTo != null && canMoveTo[to.getX()][to.getY()]) {
                if (chessTypeseses[to.getX()][to.getY()] != null) {
                    if (chs.getColour().equals(chessTypeseses[to.getX()][to.getY()].getColour())) {
                        return;
                    }
                    chessTypeseses[to.getX()][to.getY()].onDeath();
                }
                chessTypeseses[from.getX()][from.getY()] = null;
                chessTypeseses[to.getX()][to.getY()] = chs;
                nextTurn();
            }
        }
    }

    public void nextTurn() {
        if (this.turn.equals("white")) {
            this.turn = "black";
        } else if (this.turn.equals("black")) {
            this.turn = "white";
        }
    }

    public boolean[][] getCollisionsFor(Position pos) {
        boolean[][] toret = new boolean[tiles][tiles];
        for (int i = 0; i < tiles; i++) {
            for (int j = 0; j < tiles; j++) {
                toret[i][j] = (chessTypeseses[i][j] != null);
            }
        }
        if (pos != null) {
            toret[pos.getX()][pos.getY()] = false;
        }
        return toret;
    }
}
