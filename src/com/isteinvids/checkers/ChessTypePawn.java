package com.isteinvids.checkers;

import isteinvids.swinggui.Position;

/**
 *
 * @author Emir
 */
public class ChessTypePawn extends ChessTypes {

    protected ChessTypePawn(String colour) {
        super("PAWN", colour);
    }

    @Override
    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        boolean[][] toret = new boolean[8][8];
        int move = 1;
        boolean doublemove = false;
        if ((isWhite() && pos.getY() == 0) || (isBlack() && pos.getY() == 7)) {
            return toret;
        }
        if ((isWhite() && pos.getY() == 6) || (isBlack() && pos.getY() == 1)) {
            doublemove = true;
        }
        if (isWhite()) {
            move = -move;
        }
        toret[pos.getX()][pos.getY() + move] = true;
        if (doublemove) {
            toret[pos.getX()][pos.getY() + (move * 2)] = true;
        }
        for (int i = -1; i <= 1; i++) {
            ChessTypes type = Checkers.getInstance().getChessTypeAt(pos.getX() + i, pos.getY() + move);
            if (type != null && !type.getColour().equals(this.getColour())) {
                toret[pos.getX() + i][pos.getY() + move] = (i != 0);
            }
        }

        //white 6, black 1
        return toret;
    }

}
