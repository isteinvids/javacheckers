package com.isteinvids.checkers;

import isteinvids.swinggui.GameException;
import isteinvids.swinggui.Position;
import java.awt.image.BufferedImage;

/**
 *
 * @author Emir
 */
public class ChessTypes {

    public static final ChessTypes BLACK_KING = new ChessTypeKing("black");
    public static final ChessTypes BLACK_TOWER = new ChessTypeTower("black");
    public static final ChessTypes BLACK_QUEEN = new ChessTypeQueen("black");
    public static final ChessTypes BLACK_PAWN = new ChessTypePawn("black");
    public static final ChessTypes BLACK_KNIGHT = new ChessTypeKnight("black");
    public static final ChessTypes BLACK_BISHOP = new ChessTypeBishop("black");
    //    whites
    public static final ChessTypes WHITE_KING = new ChessTypeKing("white");
    public static final ChessTypes WHITE_TOWER = new ChessTypeTower("white");
    public static final ChessTypes WHITE_QUEEN = new ChessTypeQueen("white");
    public static final ChessTypes WHITE_PAWN = new ChessTypePawn("white");
    public static final ChessTypes WHITE_KNIGHT = new ChessTypeKnight("white");
    public static final ChessTypes WHITE_BISHOP = new ChessTypeBishop("white");

    private BufferedImage image = null;
    private final String name;
    private final String colour;

    protected ChessTypes(String name, String colour) {
        this.name = colour.toUpperCase() + "_" + name.toUpperCase();
        this.colour = colour;
    }

    public BufferedImage getImage() {
        if (this.image == null) {
            try {
                String ff = "data/" + name().toLowerCase() + ".png";
                this.image = Checkers.getInstance().getGameContainer().readImage(ff);
            } catch (GameException ex) {
                ex.printStackTrace();
            }
        }
        return image;
    }

    public void onDeath() {
    }

    public Position[] validMoves(Position currentPosition) {
        return new Position[0];
    }

    public final String name() {
        return name;
    }

    public final String getColour() {
        return colour;
    }

    public final boolean isBlack() {
        return colour.equals("black");
    }

    public final boolean isWhite() {
        return colour.equals("white");
    }

    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        return null;
    }

    public boolean sameColourOn(int x, int y, Position ignore) {
        if (ignore != null && ignore.getX() == x && ignore.getY() == y) {
            return false;
        }
        ChessTypes type = Checkers.getInstance().getChessTypeAt(x, y);
        if (type != null && type.getColour().equals(this.getColour())) {
            return true;
        }
        return false;
    }
}
