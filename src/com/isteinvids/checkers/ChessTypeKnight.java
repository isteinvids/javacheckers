package com.isteinvids.checkers;

/**
 *
 * @author Emir
 */
public class ChessTypeKnight extends ChessTypes {

    protected ChessTypeKnight(String colour) {
        super("KNIGHT", colour);
    }
}
