package com.isteinvids.checkers;

import isteinvids.swinggui.Position;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Emir
 */
public class ChessTypeQueen extends ChessTypes {

    protected ChessTypeQueen(String colour) {
        super("QUEEN", colour);
    }

    @Override
    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        boolean[][] toret = new boolean[8][8];
        boolean flag = false;
        for (int i = pos.getX(); i >= 0; i--) {
            if(sameColourOn(i, pos.getY(),pos))flag=true;
            if (!flag) {
                if (pos.getX() != i) {
                    toret[i][pos.getY()] = true;
                }
            }
            if (collisions[i][pos.getY()]) {
                flag = true;
            }
        }
        flag = false;
        for (int j = pos.getY(); j >= 0; j--) {
            if(sameColourOn(pos.getX(), j,pos))flag=true;
            if (!flag) {
                if (pos.getY() != j) {
                    toret[pos.getX()][j] = true;
                }
            }
            if (collisions[pos.getX()][j]) {
                flag = true;
            }
        }
        flag=false;
        for (int i = pos.getX(); i < 8; i++) {
            if(sameColourOn(i, pos.getY(),pos))flag=true;
            if (!flag) {
                if (pos.getX() != i) {
                    toret[i][pos.getY()] = true;
                }
            }
            if (collisions[i][pos.getY()]) {
                flag = true;
            }
        }
        flag = false;
        for (int j = pos.getY(); j < 8; j++) {
            if(sameColourOn(pos.getX(), j,pos))flag=true;
            if (!flag) {
                if (pos.getY() != j) {
                    toret[pos.getX()][j] = true;
                }
            }
            if (collisions[pos.getX()][j]) {
                flag = true;
            }
        }
        flag = false;
        for (int i = 0; i < 8 - pos.getX() && i < 8 - pos.getY(); i++) {
            if(sameColourOn(pos.getX()+i, pos.getY()+i,pos))flag=true;
            if (!flag) {
                toret[pos.getX() + i][pos.getY() + i] = true;
            }
            if (collisions[pos.getX() + i][pos.getY() + i]) {
                flag = true;
            }
        }
        flag = false;
        for (int i = 0; i <= Math.min(pos.getX(), pos.getY()); i++) {
            if(sameColourOn(pos.getX()-i, pos.getY()-i,pos))flag=true;
            if (!flag) {
                toret[pos.getX() - i][pos.getY() - i] = true;
            }
            if (collisions[pos.getX() - i][pos.getY() - i]) {
                flag = true;
            }
        }
        flag = false;
        for (int i = 0; i < 8 - pos.getX(); i++) {
            if(sameColourOn(pos.getX()+i, pos.getY()-i,pos))flag=true;
            if (pos.getY() - i >= 0) {
                if (!flag) {
                    toret[pos.getX() + i][pos.getY() - i] = true;
                }
                if (collisions[pos.getX() + i][pos.getY() - i]) {
                    flag = true;
                }
            }
        }
        flag = false;
        for (int i = 0; i < 8 - pos.getY(); i++) {
            if(sameColourOn(pos.getX()-i, pos.getY()+i,pos))flag=true;
            if (pos.getX() - i >= 0) {
                if (!flag) {
                    toret[pos.getX() - i][pos.getY() + i] = true;
                }
                if (collisions[pos.getX() - i][pos.getY() + i]) {
                    flag = true;
                }
            }
        }
        return toret;
    }

}
