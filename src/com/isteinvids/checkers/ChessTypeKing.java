package com.isteinvids.checkers;

import isteinvids.swinggui.Position;

/**
 *
 * @author Emir
 */
public class ChessTypeKing extends ChessTypes {

    protected ChessTypeKing(String colour) {
        super("KING", colour);
    }

    @Override
    public boolean[][] canMoveTo(Position pos, boolean[][] collisions) {
        boolean[][] toret = new boolean[8][8];
        boolean flag = false;
        if (pos.getY() + 1 < 8) {
            if (!sameColourOn(pos.getX(), pos.getY() + 1, pos)) {
                toret[pos.getX()][pos.getY() + 1] = true;
            }
        }
        if (pos.getY() - 1 >= 0) {
            if (!sameColourOn(pos.getX(), pos.getY() - 1, pos)) {
                toret[pos.getX()][pos.getY() - 1] = true;
            }
        }
        if (pos.getX() + 1 < 8) {
            if (!sameColourOn(pos.getX() + 1, pos.getY(), pos)) {
                toret[pos.getX() + 1][pos.getY()] = true;
            }
        }
        if (pos.getX() - 1 >= 0) {
            if (!sameColourOn(pos.getX() - 1, pos.getY(), pos)) {
                toret[pos.getX() - 1][pos.getY()] = true;
            }
        }
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (pos.getX() + i < 8 && pos.getY() + j < 8 && pos.getX() + i >= 0 && pos.getY() + j >= 0) {
                    if (!sameColourOn(pos.getX() + i, pos.getY() + j, pos)) {
                        toret[pos.getX() + i][pos.getY() + j] = true;
                    }
                }
            }
        }
        return toret;
    }
}
